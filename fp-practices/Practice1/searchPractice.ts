function binarySearch(input: number[], target: number) {
    // Recursive Binary => Filter Empty => Test Middle => Recursive
    return recursiveBinarySearch(input, target, 0, input.length - 1);
};

const recursiveBinarySearch = (input: number[], target: number, startIndex: number, endIndex: number): number => {
    return filterEmptyArray(input, target, startIndex, endIndex);
};

const filterEmptyArray = (input: number[], target: number, startIndex: number, endIndex: number) => {
    return (startIndex > endIndex) ? -1 : testMiddleIndexEquality(input, target, startIndex, endIndex)
};

const testMiddleIndexEquality = (input: number[], target: number, startIndex: number, endIndex: number) => {
    const middleIndex = Math.floor((startIndex + endIndex) / 2);
    const middleValue = input[middleIndex];
    return (target === middleValue) ? middleIndex : binarySearchHelper(input, target, startIndex, endIndex)
};

const binarySearchHelper = (input: number[], target: number, startIndex: number, endIndex: number) => {
    const middleIndex = Math.floor((startIndex + endIndex) / 2);
    const middleValue = input[middleIndex];
    return (target < middleValue)
        ? recursiveBinarySearch(input, target, startIndex, middleIndex - 1)
        : recursiveBinarySearch(input, target, middleIndex + 1, endIndex)
};

export = binarySearch;