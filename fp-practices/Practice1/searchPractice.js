"use strict";
function binarySearch(input, target) {
    // Recursive Binary => Filter Empty => Test Middle => Recursive
    return recursiveBinarySearch(input, target, 0, input.length - 1);
}
;
const recursiveBinarySearch = (input, target, startIndex, endIndex) => {
    return filterEmptyArray(input, target, startIndex, endIndex);
};
const filterEmptyArray = (input, target, startIndex, endIndex) => {
    return (startIndex > endIndex) ? -1 : testMiddleIndexEquality(input, target, startIndex, endIndex);
};
const testMiddleIndexEquality = (input, target, startIndex, endIndex) => {
    const middleIndex = Math.floor((startIndex + endIndex) / 2);
    const middleValue = input[middleIndex];
    return (target === middleValue) ? middleIndex : binarySearchHelper(input, target, startIndex, endIndex);
};
const binarySearchHelper = (input, target, startIndex, endIndex) => {
    const middleIndex = Math.floor((startIndex + endIndex) / 2);
    const middleValue = input[middleIndex];
    return (target < middleValue)
        ? recursiveBinarySearch(input, target, startIndex, middleIndex - 1)
        : recursiveBinarySearch(input, target, middleIndex + 1, endIndex);
};
module.exports = binarySearch;
//# sourceMappingURL=searchPractice.js.map