"use strict";
const MAX_LENGTH = 50;
const MIN_NUM = -10000;
const MAX_NUM = 10000;
const TestType = {
    ZERO_LENGTH: "ZERO_LENGTH",
    NOT_FOUND: "NOT_FOUND",
    FIRST_INDEX: "FIRST_INDEX",
    LAST_INDEX: "LAST_INDEX",
    RANDOM: "RANDOM"
};
function generate(numberOfTestCases) {
    const testDict = mapTestTypeToGenerator(MAX_LENGTH, MIN_NUM, MAX_NUM);
    const testTypeArray = Object.keys(TestType).map((key) => TestType[key]);
    return Array.from({ length: numberOfTestCases })
        .map((value, index) => testTypeArray[index] || TestType.RANDOM)
        .map(testType => testDict[testType]());
}
const mapTestTypeToGenerator = (maxLength, minimum, maximum) => {
    return {
        [TestType.ZERO_LENGTH]: zeroLengthCaseGenerator(minimum, maximum),
        [TestType.NOT_FOUND]: notFoundCaseGenerator(maxLength, minimum, maximum),
        [TestType.FIRST_INDEX]: firstIndexCaseGenerator(maxLength, minimum, maximum),
        [TestType.LAST_INDEX]: lastIndexCaseGenerator(maxLength, minimum, maximum),
        [TestType.RANDOM]: normalCaseGenerator(maxLength, minimum, maximum)
    };
};
const zeroLengthCaseGenerator = (targetMinimum, targetMaximum) => () => {
    return {
        input: [],
        target: randomIntGenerator(targetMinimum, targetMaximum),
        output: -1
    };
};
const firstIndexCaseGenerator = (maxLength, targetMinimum, targetMaximum) => () => {
    const input = randomArrayGenerator(randomIntGenerator(1, maxLength), targetMinimum, targetMaximum);
    return {
        input,
        target: input[0],
        output: 0
    };
};
const lastIndexCaseGenerator = (maxLength, targetMinimum, targetMaximum) => () => {
    const input = randomArrayGenerator(randomIntGenerator(1, maxLength), targetMinimum, targetMaximum);
    return {
        input,
        target: input[input.length - 1],
        output: input.length - 1
    };
};
const notFoundCaseGenerator = (maxLength, targetMinimum, targetMaximum) => () => {
    const input = randomArrayGenerator(randomIntGenerator(1, maxLength), targetMinimum, targetMaximum);
    const target = input[input.length - 1] + 1;
    return {
        input,
        target,
        output: -1
    };
};
const normalCaseGenerator = (maxLength, targetMinimum, targetMaximum) => () => {
    const length = randomIntGenerator(1, maxLength);
    const input = randomArrayGenerator(length, targetMinimum, targetMaximum);
    const output = randomIntGenerator(0, length - 1);
    const target = input[output];
    return { input, target, output };
};
const randomArrayGenerator = (length, minimum, maximum) => {
    return Array.from({ length })
        .reduce((array) => [...array, getUniqueInt(array, minimum, maximum)], [])
        .slice()
        .sort((a, b) => a - b);
};
const getUniqueInt = (array, minimum, maximum) => {
    const next = randomIntGenerator(minimum, maximum);
    return array.indexOf(next) === -1 ? next : getUniqueInt(array, minimum, maximum);
};
const randomIntGenerator = (low, high) => {
    return Math.round(Math.random() * (high - low)) + low;
};
generate(10);
module.exports = generate;
//# sourceMappingURL=generatePractice.js.map